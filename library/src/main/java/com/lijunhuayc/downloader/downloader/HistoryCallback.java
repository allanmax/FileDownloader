package com.lijunhuayc.downloader.downloader;

/**
 * Desc:
 * Created by zhaoyl
 */
public interface HistoryCallback {
    void onReadHistory(int downloadLength, int fileSize);
}
