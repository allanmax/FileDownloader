package com.lijunhuayc.downloader.downloader;

import android.content.Context;

/**
 */
public class WolfDownloader {
    FileDownloader fileDownloader;

    public WolfDownloader(Context mContext, DownloaderConfig config) {
        this.fileDownloader = new FileDownloader(mContext);
        this.fileDownloader.setConfig(config);
    }


    public void startDownload() {
        fileDownloader.start();
    }


    public void restartDownload() {
        fileDownloader.restart();
    }


    public void exitDownload() {
        fileDownloader.exit();
    }

}
