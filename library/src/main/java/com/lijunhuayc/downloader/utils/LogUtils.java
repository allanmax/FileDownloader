package com.lijunhuayc.downloader.utils;

import android.util.Log;

/**
 */

public class LogUtils {
    private static final boolean LOG_SWITCH = true;

    public static void d(String tag, String msg) {
        if (LOG_SWITCH) {
            Log.d(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (LOG_SWITCH) {
            Log.e(tag, msg);
        }
    }

}
